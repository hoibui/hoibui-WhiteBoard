-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 01, 2018 lúc 05:48 AM
-- Phiên bản máy phục vụ: 10.1.33-MariaDB
-- Phiên bản PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `mydb`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(8, 'hoibui', 'athoibuiqt@gmail.com', '1eb396ae25bb65ce778c3d9870f4', '2018-08-26 20:59:02', '2018-08-26 20:59:02'),
(9, 'hoi', 'athoibuiqt@gmail.com', 'aa1ab82ae3d92a2910624d5d45f8', '2018-08-26 21:03:55', '2018-08-26 21:03:55'),
(10, 'a', 'a@gmail.com', '68d9fc1706baeb1aa582d52034c7', '2018-08-26 21:06:07', '2018-08-26 21:06:07'),
(11, 'nguyet', 'demo@gmail.com', '0dd06b7ebd52223f0a61cca14081', '2018-08-26 21:28:00', '2018-08-26 21:28:00'),
(12, 'khoi', 'khoi@gmail.com', '2ad5f21ede99b3a2259055', '2018-09-01 10:21:20', '2018-09-01 10:21:20'),
(13, 'hoi', 'hoi@gmail.com', '9cf042086d73363d835255', '2018-09-01 10:22:22', '2018-09-01 10:22:22');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
